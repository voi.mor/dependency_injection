---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')

---

![bg](https://asifulhaque.com/wp-content/uploads/2021/02/dependency-injection.jpg)

---

# Gliederung
- Überblick
- Vorteile von Dependency Injection
- Vergleich mit Strategy Pattern
- Beispiel mit Code
- Übungsaufgabe 1
- Bonus Aufgabe 2
- Quellen

---
# Links
- [https://replit.com/@MorVoi/dependencyinjection](https://replit.com/@MorVor/dependencyinjection)
- [https://gitlab.com/voi.mor/dependency_injection](https://gitlab.com/voi.mor/dependency_injection)

---
# Überblick

- Creational Pattern
- Algorithmen sollen austauschbar sein: `interface` wird verwendet
- die "Dependency" Klasse implementiert das Interface und enthält den Algorithmus
- Dependency wird z.B. über den Konstruktor einer anderen Klasse hinzugefügt

---
# Vorteile von Dependency Injection

- Große Hilfe bei Unit Tests
- Macht das Erweitern des Codes einfacher
- Lose Kopplung/Single-Responibility-Prinzip
- Konfigurierbarkeit(auch zur Betriebszeit)

---
# Code Beispiel
~~~
class Car {
  private Engine engine;
  public Car(Engine engine) {
    this.engine = engine
  }
  public void start() {
    engine.start();
  }
}
interface Engine {
  void start();
}
class ReallyFastEngine implements Engine {
  @Override
  public void start() {
    System.out.println("The Engine goes vroooommmmm!!!!1!!11!!");
  }
}
~~~
---
# Strategy Pattern
~~~
interface VideoLoader {
  Video loadVideo(String name);
}
class RegularVideoLoader implements VideoLoader {
  public Video loadVideo(String name) {
    return Video.load(name).compress();
  }
}
class EvilVideoLoader implements VideoLoader {
  public Video loadVideo(String name) {
    Thread.sleep(5000);
    return Video.load(name);
  }
}
class Youtube {
  public static Video serveVideo(User user, String videoName) {
    VideoLoader loader = new RegularVideoLoader();
    if (user.isUsingAdblock()) {
      loader = EvilVideoLoader();
    }
    return loader.loadVideo(videoName);
  }
}
~~~
---
# Beispiel: Ecommerce App


![uml_diagram h:600px](uml_ecommerce_start.png)

---
# Beispiel: Ecommerce App

- Zentral: Discountberechnung
- `PriceCalculator` wird durch **Dependency Injection** `Checkout`hinzugefügt
- Art der Preisberechnung unabhängig von `Checkout`
  &rarr; kann ausgetauscht werden
- Mehrere Preisformeln möglich

---

# Code

~~~
interface PriceCalculator {
  double calculatePrice(Order order);
}

class Checkout {
  private PriceCalculator calculator;
  private Order order;
  //...
  public double getPrice() {
      return calculator.getPrice(order);
  } 
}

class BasicDiscount implements PriceCalculator {
  
  @Override
  public double getPrice(Order order) {
    // Gesamtpreis mit Discount berechnen
  }
}
~~~

---
# Aufgabe 1
## Erweitere das Ecommerce Programm mit einer weiteren Preisberechnung

- implementiere die Klasse `FlatDiscount` als `PriceCalcualator`: Der Benutzer verwendet z.B. ein Gutschien, vom Gesamtpreis wird ein bestimmer Wert abgezogen.
  Endpreis soll nicht kleiner sein als 0.

- verwende **Dependency Injection** um in `Checkout` die neue Preisformel zu verwenden
  &rarr; statische Methode `aufg1` in `Checkout` anpassen
- auf replit: klicke den `Run` button

---
# Bonus Aufgabe 2
## Implementiere die Auswahl der Preisformel mithilfe des Strategy Patterns

- verwende dazu die statischen methoden in `OtherCheckout` sowie `User`
- `main` Methode in `Checkout` anpassen
- auf replit: klicke den `Run` button

---

# Quellen

- [Wikipedia](https://en.wikipedia.org/wiki/Software_design_pattern)
- [Free Code Camp DI](https://www.freecodecamp.org/news/a-quick-intro-to-dependency-injection-what-it-is-and-when-to-use-it-7578c84fa88f/)
- [Free Code Camp Strategy Pattern](https://www.freecodecamp.org/news/a-beginners-guide-to-the-strategy-design-pattern/)
- [Bild](https://asifulhaque.com/wp-content/uploads/2021/02/dependency-injection.jpg)
- [refactoring guru](https://refactoring.guru/design-patterns/strategy)