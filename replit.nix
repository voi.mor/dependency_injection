{ pkgs }: {
    deps = [
        pkgs.graalvm17-ce
        pkgs.maven
        pkgs.nodejs_18
        pkgs.python312
        pkgs.java-language-server
    ];
}