package com.example.ecommerce;

public interface PriceCalculator {

    double getPrice(Order order);
}
