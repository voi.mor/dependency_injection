package com.example.ecommerce.loesung;

import com.example.ecommerce.*;

public class OtherCheckout {

  private static PriceCalculator getPriceFormula(User user) {
    if (user.getHasPremiumSubscription()) {
      return new PremiumSubscriptionDiscount(user);
    } else {
      return new BasicDiscount(0.0);
    }
  }

  public static double calculatePrice(Order order, User user){
      return getPriceFormula(user).getPrice(order);
  }
}