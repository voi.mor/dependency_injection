package com.example.ecommerce.loesung;

import com.example.ecommerce.Order;
import com.example.ecommerce.PriceCalculator;
import com.example.ecommerce.Product;

public class FlatDiscount implements PriceCalculator {

  private double discount;

  @Override
  public double getPrice(Order order) {
      double sum = 0;
      for (Product p : order.getProducts()) {
          sum += p.getPrice();
      }

      sum -= discount;
      return Math.max(0, sum);
  }

  public FlatDiscount(double discount) {
    this.discount = discount;
  }
  
} 