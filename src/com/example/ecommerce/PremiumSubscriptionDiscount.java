package com.example.ecommerce;

import java.util.Comparator;
import java.util.List;

public class PremiumSubscriptionDiscount implements PriceCalculator {

    private User user;

    public PremiumSubscriptionDiscount(User user) {
        this.user = user;
    }

    @Override
    public double getPrice(Order order) {
        List<Product> sortedProducts = order.getProducts();
        sortedProducts.sort((p1, p2) ->Double.compare(p2.getPrice(), p1.getPrice()));
        double sum = 0;
        for (Product p : sortedProducts) {
            if (user.getFreeProductsRemaining() > 0) {
                user.decrementFreeProducts();
            } else {
                sum += p.getPrice();
            }
        }
        return sum;
    }
}
