package com.example.ecommerce;

public class User {
  private String name;
  private boolean hasPremiumSubscription;
  private int freeProductsRemaining;

  public User(String name, boolean hasPremium, int freeProducts) {
    this.name = name;
    this.hasPremiumSubscription = hasPremium;
    this.freeProductsRemaining = freeProducts;
  }

  public void givePremiumSubscription() {
    hasPremiumSubscription = true;
    freeProductsRemaining = 3;
  }

  public void decrementFreeProducts() {
    freeProductsRemaining--;
  }

  public boolean getHasPremiumSubscription() {
    return this.hasPremiumSubscription;
  }

  public int getFreeProductsRemaining() {
    return freeProductsRemaining;
  }
  
}