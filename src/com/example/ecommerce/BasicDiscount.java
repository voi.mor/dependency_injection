package com.example.ecommerce;

public class BasicDiscount implements PriceCalculator {

    private double discountRate;

    public BasicDiscount(double discountRate) {
        this.discountRate = discountRate;
    }

    @Override
    public double getPrice(Order order) {
        double sum = 0;
        for (Product p : order.getProducts()) {
            sum += p.getPrice();
        }
        return sum * (1 - discountRate);
    }


}
