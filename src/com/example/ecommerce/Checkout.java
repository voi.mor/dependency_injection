package com.example.ecommerce;

public class Checkout {
    private Order order;
    private PriceCalculator calculator;

    public Checkout(Order order, PriceCalculator calculator) {
        this.order = order;
        this.calculator = calculator;
    }

    public double calculatePrice() {
        return calculator.getPrice(order);
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public PriceCalculator getCalculator() {
        return calculator;
    }

    public void setCalculator(PriceCalculator calculator) {
        this.calculator = calculator;
    }

    public static void main(String... args) {
      aufg1();
      // dies für aufgabe 2 wieder einfügen
      //aufg2();
    }

    public static void aufg1() {
        System.out.println("--------------------");
        Order order = new Order();
        order.addProduct(new Product("hose", 25));
        order.addProduct(new Product("schuhe", 75));
        double discount = 0.15;
        Checkout checkout = new Checkout(order, new BasicDiscount(discount));
        System.out.println("Benutzer kauft " + order);
        System.out.printf("Preis mit %s prozent discount: %s\n", discount, checkout.calculatePrice());
        System.out.println("---");
        Order order2 = new Order();
        order2.addProduct(new Product("radiergummi", .5));
        order2.addProduct(new Product("buntstifte", 3));
        User hansi = new User("hansi", true, 1);
        Checkout checkout2 = new Checkout(order2, new PremiumSubscriptionDiscount(hansi));
        System.out.println("Benutzer kauft " + order2);
        System.out.printf("Preis mit 1 gratis produkten: %s\n", checkout2.calculatePrice());

        Order order3 = new Order();
        order3.addProduct(new Product("schulranzen", 40));
        order3.addProduct(new Product("laptop", 300));
        // Hier FlatDiscount verwenden
         // Checkout checkout3 = new Checkout(order3, new FlatDiscount(50));
         // System.out.println("Benutzer kauft " + order3);
         // System.out.printf("Preis mit 50 eure gutschein: %s\n", checkout3.calculatePrice());
      
        System.out.println("-------------------------------");
    }

    public static void aufg2() {
      User user = new User("horst", false, 0);
      Order order = new Order();
      order.addProduct(new Product("schulranzen", 40));
      order.addProduct(new Product("laptop", 300));
      order.addProduct(new Product("laptop", 300));
      order.addProduct(new Product("laptop", 300));
      order.addProduct(new Product("radiergummi", .5));
      order.addProduct(new Product("buntstifte", 3));
      System.out.println("Benutzer kauft " + order);
      System.out.println("Ohne Premium " + OtherCheckout.calculatePrice(order, user));
      user.givePremiumSubscription();
      System.out.println("Mit Premium " + OtherCheckout.calculatePrice(order, user));
    }
}
