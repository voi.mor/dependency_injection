package com.example.ecommerce;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private int id;
    private final List<Product> products = new ArrayList<>();

    public Order() {
    }

    public void addProduct(Product p) {
        products.add(p);
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Order{" +
                "products=" + products +
                ", totalPriceNoDiscount=" + products.stream().map(Product::getPrice).reduce(Double::sum).orElse(0.0) +
                '}';
    }
}
